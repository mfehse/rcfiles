# Path to your oh-my-zsh installation.
  export ZSH=/home/${USERNAME}/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
if [ -d  "$ZSH/themes/powerlevel9k" -o -d "$ZSH/custom/themes/powerlevel9k" ]; then
	# WARNING! Your terminal appears to support less than 256 colors!
	# If your terminal supports 256 colors, please export the appropriate environment variable
	# _before_ loading this theme in your ~/.zshrc. In most terminal emulators, putting
	# export TERM="xterm-256color" at the top of your ~/.zshrc is sufficient.
	TERM="xterm-256color"
	POWERLEVEL9K_MODE='nerdfont-complete'
	ZSH_THEME="powerlevel9k/powerlevel9k"
elif [ -d "/usr/share/zsh-theme-powerlevel9k" ]; then
	ZSH_CUSTOM="/usr/share/zsh-theme-powerlevel9k"
	ZSH_THEME="powerlevel9k"
elif [ -f "$ZSH/themes/ys+.zsh-theme" -o -f "$ZSH/custom/themes/ys+.zsh-theme" ]; then
	ZSH_THEME="ys+"
else
	ZSH_THEME="agnoster"
fi

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=31

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(archlinux git common-aliases compleat history-substring-search z)

# User configuration

# export PATH="/usr/bin:/bin:/usr/sbin:/sbin:$PATH"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi
export EDITOR='vim'
export VISUAL='vim'

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias cal='ncal -w3M'
alias cdw='cd /var/www/users/${USERNAME}/'
alias ll='ls -lhF --group-directories-first'
alias la='ls -a'
alias ldir='ls -d */'
alias l='ls -CF'
alias nano='nano --tabsize=2 --smooth --const --autoindent --undo --softwrap'
alias saidar='saidar -c'
alias v='vim'

eval $(thefuck --alias)

export NVM_DIR="/home/${USERNAME}/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
