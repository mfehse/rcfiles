# rcfiles (dotfiles) of matthias

## dependencies
[thefuck](https://github.com/nvbn/thefuck) has to be installed. this has to be done like the following:

```terminal
$ sudo apt-get install python-pip python-dev
$ sudo pip install psutil thefuck
```

## installation
checkout this repo into a folder e.g. ~/.rcfiles:

```terminal
$ cd
$ git clone https://bitbucket.org/mfehse/rcfiles .rcfiles
```

## included stuff

### bash

the default linux-shell i used for years

```terminal
$ ln -s ~/.rcfiles/.bashrc ~/.bashrc
$ source ~/.bashrc
```

### git

just create these links within home-directory:

```terminal
$ ln -s ~/.rcfiles/.gitconfig ~/.gitconfig
$ ln -s ~/.rcfiles/.git-commit-tpl ~/.git-commit-tpl
```

### mercurial

just create these links within home-directory:

```terminal
$ ln -s ~/.rcfiles/.hgrc ~/.hgrc
```

### midnight commander

great and useful file-manager

```terminal
$ ln -s ~/.rcfiles/mc/themes/darkened.ini ~/.local/share/mc/skins/darkened.ini
$ ln -s ~/.rcfiles/mc/ini ~/.config/mc/ini
```

### mutt
> mutt is a text-based email client for unix-like systems. [wikipedia.org](https://en.wikipedia.org/wiki/Mutt_(email_client))
my configuration contains one account with these placeholders:

```terminal
$ ln -s ~/.rcfiles/.muttrc ~/.muttrc
$ cp ~/.rcfiles/.mutt ~/
```

following placeholders should be replaced within *.muttrc* according to your setup:

- EMAIL
- PASSWORD
- DOMAIN
- PORT
- COMPANY

### nano

config-file for a simple text-editor..

```terminal
$ ln -s ~/.rcfiles/.nanorc ~/.nanorc
```

### nvm

the [node version manager](https://github.com/creationix/nvm)

install via
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash
```
no link neccessary cause it's appended within .bashrc / .zshrc

### terminator

[terminal-multiplexer](https://launchpad.net/terminator)

```terminal
$ ln -s ~/.rcfiles/.terminator.config ~/.config/terminator/config
```

### vim
base-configuration is done by [spf13-vim](https://github.com/spf13/spf13-vim). afterwards create the links..

```terminal
$ ln -s ~/.rcfiles/.vimrc.local ~/.vimrc.local
$ ln -s ~/.rcfiles/.vimrc.before.local ~/.vimrc.before.local
$ ln -s ~/.rcfiles/.vimrc.bundles.local ~/.vimrc.bundles.local
```

### visual studio code
microsoft's [ide](https://code.visualstudio.com/).
```terminal
$ ln -s ~/.rcfiles/code/keybindings.json ~/.config/Code/User/keybindings.json
$ ln -s ~/.rcfiles/code/settings.json ~/.config/Code/User/settings.json
```

### z-shell / oh-my-zsh

just install [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh) on top of zsh - imho it's the better shell ;-).

```terminal
$ ln -s ~/.rcfiles/zsh/.zshrc ~/.zshrc
$ ln -s ~/.rcfiles/zsh/themes/ys+.zsh-theme ~/.oh-my-zsh/themes/ys+.zsh-theme
$ source ~/.zshrc
```

for best ux install [nerd-fonts](https://github.com/ryanoasis/nerd-fonts) and select `Droid Sans Mono for Powerline Nerd` as terminal-font:
```terminal
$ cd ~/.local/share/fonts && curl -fLo "Droid Sans Mono for Powerline Nerd Font Complete.otf" https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20for%20Powerline%20Nerd%20Font%20Complete.otf
```

## contact
maintainer: matthias fehse  
email: mfehse@ibse-fehse.de

